﻿using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using ProjectTestMediator.Application.Context;
using SampleTask.Application.Business;
using SampleTask.Application.Data;
using SampleTask.Application.Models;
using SampleTask.Application.Services;
using SampleTask.Filter;
using SampleTask.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SampleTask
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddHttpContextAccessor();
            services.AddApiVersioning();

            services.AddApiVersioning(opt =>
            {
                opt.AssumeDefaultVersionWhenUnspecified = true;
                opt.DefaultApiVersion = new ApiVersion(1, 0);
            });

            services.AddOptions<BearerTokensOptions>()
              .Bind(Configuration.GetSection("BearerTokens"))
              .Validate(bearerTokens =>
              {
                  return true;
                   // return bearerTokens.AccessTokenExpirationMinutes < bearerTokens.RefreshTokenExpirationMinutes;
               }, "RefreshTokenExpirationMinutes is less than AccessTokenExpirationMinutes. Obtaining new tokens using the refresh token should happen only if the access token has expired.");




            services.Configure<ApiBehaviorOptions>(options => {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddMediatR(typeof(Startup));

            services.AddTransient<ITokenService, TokenService>();

            services.AddTransient<UserResolverService>();
            services.AddScoped<IUnitOfWork, SampleTaskDbContext>();
            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));


            //services.AddDbContext<SampleTaskDbContext>(options =>
            //     options.UseSqlServer("Server=.;Database=TestInterview;Integrated Security=True"));



            ///use in memory
           services.AddDbContext<SampleTaskDbContext>(opt => opt.UseInMemoryDatabase(databaseName: "SampleDb"), ServiceLifetime.Scoped, ServiceLifetime.Scoped);

            services.AddScoped<IUserRepository,UserRepository>();
            services.AddScoped<IUserGeographicalPointRepository, UserGeographicalPointRepository>();


            
            services.AddScoped<ModelStateValidationFilter>();

            services.AddScoped<IGeoCalculate,GeoCalculate>();


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, cfg =>
             {

                 var secretkey = Encoding.UTF8.GetBytes(Configuration["BearerTokens:Key"]);
                 var encryptionkey = Encoding.UTF8.GetBytes(Configuration["BearerTokens:EncryptKey"]);
                 String ss = Configuration["BearerTokens:Issuer"];
                 cfg.RequireHttpsMetadata = false;
                 cfg.SaveToken = true;
                 cfg.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidIssuer = Configuration["BearerTokens:Issuer"], // site that makes the token
                     ValidateIssuer = false, // TODO: change this to avoid forwarding attacks
                     ValidAudience = Configuration["BearerTokens:Audience"], // site that consumes the token
                     ValidateAudience = false, // TODO: change this to avoid forwarding attacks
                     ValidateLifetime = true, // validate the expiration
                     ValidateIssuerSigningKey = true,
                     IssuerSigningKey = new SymmetricSecurityKey(secretkey),
                     RequireSignedTokens = true,
                     ClockSkew = TimeSpan.Zero, // tolerance for the expiration date
                     TokenDecryptionKey = new SymmetricSecurityKey(encryptionkey)
                 };
                 cfg.Events = new JwtBearerEvents
                 {
                     OnAuthenticationFailed = context =>
                     {
                         if (context.Exception is Exception)
                         {
                             //set this state makes it works. I got 440 statuscode in Postman.
                             //context.State = Microsoft.AspNetCore.Authentication.EventResultState.HandledResponse;
                             context.Response.StatusCode = 440;
                         }
                         //   var logger = context.HttpContext.RequestServices.GetRequiredService<ILoggerFactory>().CreateLogger(nameof(JwtBearerEvents));
                         //    logger.LogError("Authentication failed.", context.Exception);
                         return Task.CompletedTask;
                     },
                     OnTokenValidated = context =>
                     {

                         var tokenValidatorService = context.HttpContext.RequestServices.GetRequiredService<ITokenService>();
                         return tokenValidatorService.ValidateAsync(context);
                     },
                     OnMessageReceived = context =>
                     {
                         return Task.CompletedTask;
                     },
                     OnForbidden = context =>
                     {
                         return Task.CompletedTask;

                     },
                     OnChallenge = context =>
                     {
                         context.HandleResponse();

                         context.Response.ContentType = "application/json";
                         context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                         context.Response.WriteAsync(new ResponseObject()
                         {
                             status = context.Response.StatusCode,
                             msg = "عدم دسترسی"
                         }.ToString());
                         return Task.CompletedTask;
                     }


                 };

             });




            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SampleTask", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
      {
        {
          new OpenApiSecurityScheme
          {
            Reference = new OpenApiReference
              {
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer"
              },
              Scheme = "oauth2",
              Name = "Bearer",
              In = ParameterLocation.Header,

            },
            new List<string>()
          }
        });
            //    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            //    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
             //   c.IncludeXmlComments(xmlPath);
           


        });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SampleTask v1"));
            }

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<SampleTaskDbContext>();
                context.Database.EnsureCreated();
            }

            app.UseRouting();
           // app.UseCors("CorsPolicy");
            app.UseMiddleware<ErrorHandlingMiddleware>();


            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
