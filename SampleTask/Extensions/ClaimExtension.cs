﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SampleTask.Extensions
{
    public static class ClaimExtension
    {
        public static long UserId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                return 0;

            if (principal.Identity == null)
                return 0;
            if (principal.Identity.IsAuthenticated != true)
                return 0;
            var userIdString = principal.FindFirst(ClaimTypes.NameIdentifier).Value;
            return Int32.Parse(userIdString);

        }

    }
}
