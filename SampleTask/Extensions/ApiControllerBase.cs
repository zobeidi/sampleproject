﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SampleTask.Application.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Extensions
{
    [ApiController]
    public abstract class ApiControllerBase : ControllerBase
    {
        private readonly ICollection<ErrorModelState> _errors = new List<ErrorModelState>();

        protected ActionResult Response(object result = null)
        {
            if (IsOperationValid())
            {

                return Ok(new ResponseObject(0, "", result));
            }

            return BadRequest(new ResponseObject { data = _errors.ToArray(), status = 400, msg = "لطفا خطاها را برطرف کنید" });

        }

        protected ActionResult BadResponse(string msg)
        {
            return BadRequest(new ResponseObject(0, msg, null));

        }

        protected ActionResult Response(ModelStateDictionary modelState)
        {
            (from ms in modelState
             where ms.Value.Errors.Any()
             select new ErrorModelState
             {
                 key = ms.Key,
                 values = ms.Value.Errors.Select(d => d.ErrorMessage).ToList()
             }
                           ).ToList().ForEach(d => _errors.Add(d));



            return Response();
        }



        protected bool IsOperationValid()
        {
            return !_errors.Any();
        }

        protected void AddError(ErrorModelState error)
        {
            _errors.Add(error);
        }

        protected void ClearErrors()
        {
            _errors.Clear();
        }
    }
}
