﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SampleTask.Application.Business;
using SampleTask.Application.Domain;
using SampleTask.Application.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SampleTask.Infrastructure
{
    public interface ITokenService
    {
        AccsessToken CreateToken(User user);
        Task ValidateAsync(TokenValidatedContext context);

    }

    public class TokenService : ITokenService
    {
        private readonly IOptionsSnapshot<BearerTokensOptions> _configuration;
        private readonly IUserRepository _userRepository;
        public TokenService(IOptionsSnapshot<BearerTokensOptions> configuration,
            IUserRepository userRepository
            )
        {

            _configuration = configuration;
            _userRepository = userRepository;

        }
        public AccsessToken CreateToken(User user)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToString(), ClaimValueTypes.Integer64, _configuration.Value.Issuer));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString(), ClaimValueTypes.String, _configuration.Value.Issuer));
            


            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.Value.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var now = DateTime.UtcNow;
            var token = new JwtSecurityToken(
                issuer: _configuration.Value.Issuer,
                audience: _configuration.Value.Audience,
                claims: claims,
                notBefore: now,
                expires: now.AddMinutes(_configuration.Value.AccessTokenExpirationMinutes),
                signingCredentials: creds);

            return new AccsessToken
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
            };
          
        }

        public async Task ValidateAsync(TokenValidatedContext context)
        {
            var userPrincipal = context.Principal;

            var claimsIdentity = context.Principal.Identity as ClaimsIdentity;
            if (claimsIdentity?.Claims == null || !claimsIdentity.Claims.Any())
            {
                context.Fail("This is not our issued token. It has no claims.");
                return;
            }

            //var serialNumberClaim = claimsIdentity.FindFirst(ClaimTypes.SerialNumber);
            //if (serialNumberClaim == null)
            //{
            //    context.Fail("This is not our issued token. It has no serial.");
            //    return;
            //}

            var userIdString = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (!int.TryParse(userIdString, out int userId))
            {
                context.Fail("This is not our issued token. It has no user-id.");
                return;
            }

            var user = await _userRepository.Get(d => d.Id == userId);
            if (user == null || /*user.SerialNumber != serialNumberClaim.Value ||*/ !user.IsActive)
            {
                // user has changed his/her password/roles/stat/IsActive
                context.Fail("This token is expired. Please login again.");
                return;
            }

            //var accessToken = context.SecurityToken as JwtSecurityToken;
            //if (accessToken == null || string.IsNullOrWhiteSpace(accessToken.RawData) ||
            //    !await IsValidTokenAsync(accessToken.RawData, userId))
            //{
            //    context.Fail("This token is not in our database.");
            //    return;
            //}





            ////////////Add Custome Claim and roles
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Role, "user", ClaimValueTypes.String, _configuration.Value.Issuer));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier,user.Id.ToString(), ClaimValueTypes.String, _configuration.Value.Issuer));


            ////////////Update User LastActivity
            //          await _usersService.UpdateUserLastActivityDateAsync(userId);
        }

    }

}
