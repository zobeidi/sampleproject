﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SampleTask.Application.Models;
using SampleTask.Infrastructure.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SampleTask.Infrastructure
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;

        public ErrorHandlingMiddleware(RequestDelegate next,
            ILogger<ErrorHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (System.Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, System.Exception exception)
        {
            int statusCode;
            List<String> errors = new List<string>();
            if (exception is RestException re)
            {
                statusCode = (int)re.Code;

                if (re.Message is string)
                {
                    errors.Add(re.Message.ToString());
                }
            }
            else
            {
                statusCode = (int)HttpStatusCode.InternalServerError;
                errors.Add("An internal server error has occurred." + exception.Message.ToString());
            }

            _logger.LogError($"{errors} - {exception.Source} - {exception.Message} - {exception.StackTrace} - {exception.TargetSite?.Name}");

            context.Response.StatusCode = statusCode;
            context.Response.ContentType = "application/json";

            await context.Response.WriteAsync(JsonConvert.SerializeObject(new ResponseObject { data = errors, msg = string.Join(",", errors.ToArray()) }));
        }


    }

}
