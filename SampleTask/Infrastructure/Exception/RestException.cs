﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SampleTask.Infrastructure.Exception
{
    public class RestException : System.Exception
    {
        public RestException(HttpStatusCode code, object message = default!)
        {
            Code = code;
            Message = message;
        }

        public HttpStatusCode Code { get; }

        public new object Message { get; }
    }
}
