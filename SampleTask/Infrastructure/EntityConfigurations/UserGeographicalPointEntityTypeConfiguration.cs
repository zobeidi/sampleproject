﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SampleTask.Application.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Infrastructure.EntityConfigurations
{
    class UserGeographicalPointEntityTypeConfiguration
       : IEntityTypeConfiguration<UserGeographicalPoint>
    {
        public void Configure(EntityTypeBuilder<UserGeographicalPoint> userGeographicalPointConfiguration)
        {
            userGeographicalPointConfiguration
               .OwnsOne(o => o.StartPoint, a =>
               {
                    // Explicit configuration of the shadow key property in the owned type 
                    // as a workaround for a documented issue in EF Core 5: https://github.com/dotnet/efcore/issues/20740
                   
                   a.WithOwner();
               });


            userGeographicalPointConfiguration
              .OwnsOne(o => o.EndPoint, a =>
              {
                   // Explicit configuration of the shadow key property in the owned type 
                   // as a workaround for a documented issue in EF Core 5: https://github.com/dotnet/efcore/issues/20740

                   a.WithOwner();
              });


           
        }
    }

}
