﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SampleTask.Application.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Infrastructure.EntityConfigurations
{
    class UserEntityTypeConfiguration
       : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> userEntityTypeConfiguration)
        {

            userEntityTypeConfiguration.HasMany(d => d.UserGeographicalPoints).WithOne(d => d.User).IsRequired();

        }
    }

}
