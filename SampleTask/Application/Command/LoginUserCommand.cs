﻿using MediatR;
using SampleTask.Application.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace SampleTask.Application.Command
{
    [DataContract]
    public class LoginUserCommand : IRequest<AccsessToken>, IViewModelBase
    {
    
        [Required(ErrorMessage = "لطفا ایمیل را وارد کنید")]
        [EmailAddress(ErrorMessage = "ایمیل را درست وارد کنید است")]
        public string email { set; get; }


        [Required(ErrorMessage = "رمز عبور را وارد کنید")]
        public string password { set; get; }


    }
}
