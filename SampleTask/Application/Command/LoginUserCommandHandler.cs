﻿using MediatR;
using Microsoft.Extensions.Logging;
using ProjectTestMediator.Application.Context;
using SampleTask.Application.Business;
using SampleTask.Infrastructure;
using SampleTask.Infrastructure.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using SampleTask.Application.Models;

namespace SampleTask.Application.Command
{
    public class LoginUserCommandHandler : IRequestHandler<LoginUserCommand, AccsessToken>
    {
        private readonly ILogger<RegisterUserCommandHandler> _logger;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITokenService _tokenService;

        public LoginUserCommandHandler(ILogger<RegisterUserCommandHandler> logger,
            IUserRepository userRepository,
            IUnitOfWork unitOfWork,
            ITokenService tokenService)
        {
            _logger = logger;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _tokenService = tokenService;

        }

        public async Task<AccsessToken> Handle(LoginUserCommand message, CancellationToken cancellationToken)
        {

         var currentUser=  await _userRepository.Get(d => d.Email == message.email);
            if (currentUser == null)
                throw new RestException(System.Net.HttpStatusCode.BadRequest, "user with this email not exists");

          var accessToken= _tokenService.CreateToken(currentUser);

            return accessToken;
        }


        }
}
