﻿using MediatR;
using SampleTask.Application.Domain;
using SampleTask.Application.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace SampleTask.Application.Command
{

   

    [DataContract]
    public class RegisterUserLocationCommand : IRequest<double>, IViewModelBase
    {
        public LocationView startPoint { set; get; }
        public LocationView endPoint { set; get; }
      
        [IgnoreDataMember]
        public long userId { set; get; }

    }
}
