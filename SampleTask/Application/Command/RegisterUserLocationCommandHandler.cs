﻿using MediatR;
using Microsoft.Extensions.Logging;
using ProjectTestMediator.Application.Context;
using SampleTask.Application.Business;
using SampleTask.Infrastructure.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace SampleTask.Application.Command
{
    public class RegisterUserLocationCommandHandler : IRequestHandler<RegisterUserLocationCommand, double>
    {
        private readonly ILogger<RegisterUserCommandHandler> _logger;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGeoCalculate _geocalculate;
        public RegisterUserLocationCommandHandler(ILogger<RegisterUserCommandHandler> logger,
            IUserRepository userRepository,
            IUnitOfWork unitOfWork, IGeoCalculate geocalculate)
        {
            _logger = logger;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _geocalculate = geocalculate;
        }

        public async Task<double> Handle(RegisterUserLocationCommand message, CancellationToken cancellationToken)
        {

         var currentUser=  await _userRepository.Get(d => d.Id == message.userId);

            if (currentUser == null)
                throw new RestException(System.Net.HttpStatusCode.BadRequest, "user not Exists");

            currentUser.UserGeographicalPoints.Add(new Domain.UserGeographicalPoint
            {
                IsActive = true,
                StartPoint = new Domain.Location { Latitude = message.startPoint.latitude, Longitude = message.startPoint.longitude },
                EndPoint = new Domain.Location { Latitude = message.endPoint.latitude, Longitude = message.endPoint.longitude },
             Distance= _geocalculate.GetDistance(message.startPoint,message.endPoint)
            });
         
           await _unitOfWork.SaveChangesAsync(cancellationToken);
            return currentUser.UserGeographicalPoints.First().Distance;

        }


        }
}
