﻿using MediatR;
using Microsoft.Extensions.Logging;
using ProjectTestMediator.Application.Context;
using SampleTask.Application.Business;
using SampleTask.Infrastructure.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace SampleTask.Application.Command
{
    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand,bool>
    {
        private readonly ILogger<RegisterUserCommandHandler> _logger;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RegisterUserCommandHandler(ILogger<RegisterUserCommandHandler> logger,
            IUserRepository userRepository,
            IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;

        }

        public async Task<bool> Handle(RegisterUserCommand message, CancellationToken cancellationToken)
        {

         var currentUser=  await _userRepository.Get(d => d.Email == message.email);
            if (currentUser != null)
                throw new RestException(System.Net.HttpStatusCode.BadRequest, "user with this email already exists");

            currentUser = new Domain.User { Email = message.email, Password = message.password, 
                FirstName = message.firstName, LastName = message.lastName,IsActive=true };
            _userRepository.Add(currentUser);
           await _unitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }


        }
}
