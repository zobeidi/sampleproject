﻿using Microsoft.EntityFrameworkCore;
using ProjectTestMediator.Application.Context;
using SampleTask.Application.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SampleTask.Application.Data
{

    public  class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class, IEntity, new()
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<TEntity> _dbSet;
        public RepositoryBase(IUnitOfWork uow)
        {
            _uow = uow;
            _dbSet = _uow.Set<TEntity>();

        }
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _uow;
            }
        }

        public virtual TEntity Add(TEntity entity)
        {
            return _dbSet.Add(entity).Entity;

        }
        public virtual void Update(TEntity entity)
        {
            _dbSet.Update(entity);


        }
        public virtual void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }
        public virtual  Task<TEntity> Get(Expression<Func<TEntity, bool>> filter)
        {
            return _dbSet.Where(filter).FirstOrDefaultAsync();
        }
       
        public virtual Task<List<TEntity>> GetAll()
        {
           return  _dbSet.ToListAsync();

        }

        public virtual IQueryable<TEntity> GetQueryable()
        {
            return _dbSet.AsQueryable();
        }
       


    }

}
