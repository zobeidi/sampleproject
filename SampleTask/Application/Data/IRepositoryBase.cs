﻿using ProjectTestMediator.Application.Context;
using SampleTask.Application.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SampleTask.Application.Data
{
    public interface IRepositoryBase<TEntity> where TEntity : class, new()
    {
        Task<TEntity> Get(Expression<Func<TEntity, bool>> filter);
        Task<List<TEntity>> GetAll();
        void Delete(TEntity entity);

        void Update(TEntity entity);

        IQueryable<TEntity> GetQueryable();

        TEntity Add(TEntity entity);
        IUnitOfWork UnitOfWork { get; }

    }



}
