﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Utility
{
    public interface IResult
    {
        bool Succsess { get; }
        string Message { get; }
    }
    public interface IDataResult<out T> : IResult
    {
        T Data { get; }
    }
    public class Result : IResult
    {
        public bool Succsess { get; }

        public string Message { get; }

        public Result(bool succsess, string message) : this(succsess)
        {
            Message = message;
        }
        public Result(bool succsess)
        {
            Succsess = succsess;
        }
    }
    public class DataResult<T> : Result, IDataResult<T>
    {
        public T Data { get; }
        public DataResult(T data, bool succsess, string message) : base(succsess, message)
        {
            Data = data;
        }
        public DataResult(T data, bool succsess) : base(succsess)
        {
            Data = data;
        }
    }
}
