﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Models
{
    public class UserRegisterViewModelDto:IViewModelBase
    {
        [Required(ErrorMessage ="لطفا نام را وارد کنید")]
        public string firstName { set; get; }

        [Required(ErrorMessage = "لطفا نام را وارد کنید")]
        public string lastName { set; get; }


        [Required(ErrorMessage = "لطفا ایمیل را وارد کنید")]
        [EmailAddress(ErrorMessage = "ایمیل را درست وارد کنید است")]
        public string email { set; get; }


        [Required(ErrorMessage = "رمز عبور را وارد کنید")]
        public string password { set; get; }

    }
}
