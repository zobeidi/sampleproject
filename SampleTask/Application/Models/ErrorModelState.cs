﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Models
{
    public class ErrorModelState
    {
        public ErrorModelState()
        {
            values = new List<string>();
        }
        public ErrorModelState(string Key, List<string> Values)
        {
            key = key;
            values = Values;
        }
        public string key { get; set; }
        public List<string> values { get; set; }
    }
}
