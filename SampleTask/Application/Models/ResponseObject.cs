﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Models
{
    public class ResponseObject
    {

        public ResponseObject()
        {

        }

        public ResponseObject(object obj)
        {
            data = obj;
            this.msg = "";
        }
        public ResponseObject(int status, string msg, object data)
        {
            this.status = status;
            this.msg = msg;
            this.data = data;
        }
        public int status { set; get; }
        public object data { set; get; }
        public string msg { set; get; }
        public override string ToString()
        {

            return JsonConvert.SerializeObject(this);
        }

    }

    public class ReponseModelStateError
    {

     

        public ReponseModelStateError(ModelStateDictionary modelState,string msg,int status)
        {
            List<ErrorModelState> errors = new List<ErrorModelState>();
            (from ms in modelState
             where ms.Value.Errors.Any()
             select new ErrorModelState
             {
                 key = ms.Key,
                 values = ms.Value.Errors.Select(d => d.ErrorMessage).ToList()
             }
               ).ToList().ForEach(d => errors.Add(d));

            this.data = errors;
            this.msg = msg;
            this.status = status;

        }
    
        public int status { set; get; }
        public object data { set; get; }
        public string msg { set; get; }
        public override string ToString()
        {

            return JsonConvert.SerializeObject(this);
        }

    }

}
