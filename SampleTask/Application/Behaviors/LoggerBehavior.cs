﻿using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestMediator.Core.Application.Behaviors
{
    public class EventLoggerBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        // readonly IEventStoreDbContext _eventStoreDbContext;
        private readonly ILogger<EventLoggerBehavior<TRequest, TResponse>> _logger;

        public EventLoggerBehavior(ILogger<EventLoggerBehavior<TRequest, TResponse>> logger)
        {
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            TResponse response = await next();

            string requestName = request.ToString();

            // Commands convention
            if (requestName.EndsWith("Command"))
            {
                Type requestType = request.GetType();
                string commandName = requestType.Name;

                var data = new Dictionary<string, object>
            {
                {
                    "request", request
                },
                {
                    "response", response
                }
            };

                string jsonData = JsonConvert.SerializeObject(data);
              //  byte[] dataBytes = Encoding.UTF8.GetBytes(jsonData);
                _logger.LogInformation(jsonData);
           
                ///////Add log to evenstore or elestic search
                //EventData eventData = new EventData(eventId: Guid.NewGuid(),
                //    type: commandName,
                //    isJson: true,
                //    data: dataBytes,
                //    metadata: null);

                //await _eventStoreDbContext.AppendToStreamAsync(eventData);
            }

            return response;
        }
    }
}
