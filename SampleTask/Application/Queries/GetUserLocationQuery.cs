﻿
using MediatR;
using SampleTask.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace SampleTask.Application.Queries
{
    public class PaginatedItemsViewModel<TEntity> where TEntity : class
    {
        public int PageIndex { get; private set; }

        public int PageSize { get; private set; }

        public long Count { get; private set; }

        public IEnumerable<TEntity> Data { get; private set; }

        public PaginatedItemsViewModel(int pageIndex, int pageSize, long count, IEnumerable<TEntity> data)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            Count = count;
            Data = data;
        }
    }



    public class UserLocationListItemViewModel
    {
        public LocationView startPoint { set; get; }
        public LocationView endPoint { set; get; }
        public double distance { set; get; }
    }



    public class GetUserLocationQuery : IRequest<PaginatedItemsViewModel<UserLocationListItemViewModel>>
    {
        public GetUserLocationQuery(int userId, string sort = "", int pageSize = 10, int pageIndex = 0)
        {
            this.pageSize = pageSize;
            this.pageIndex = pageIndex;
            this.sort = sort;
            this.userId = userId;
        }

        public GetUserLocationQuery()
        {
        }
        [IgnoreDataMember]
        public long userId { set; get; }
        public string sort { set; get; }
        public int pageSize { set; get; }
        public int pageIndex { set; get; }
    }





}
