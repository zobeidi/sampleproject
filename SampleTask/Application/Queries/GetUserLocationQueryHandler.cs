﻿
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ProjectTestMediator.Application.Context;
using SampleTask.Application.Business;
using SampleTask.Application.Models;
using SampleTask.Infrastructure.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SampleTask.Application.Queries
{
   
        public class GetUserLocationQueryHandler : IRequestHandler<GetUserLocationQuery, PaginatedItemsViewModel< UserLocationListItemViewModel>>
        {

        private readonly ILogger<GetUserLocationQueryHandler> _logger;
        private readonly IUserRepository _userRepository;
        private readonly IUserGeographicalPointRepository _userGeographicalPointRepository;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IGeoCalculate _geocalculate;
        public GetUserLocationQueryHandler(ILogger<GetUserLocationQueryHandler> logger,
            IUserRepository userRepository,
            IUnitOfWork unitOfWork, IGeoCalculate geocalculate, IUserGeographicalPointRepository userGeographicalPointRepository)
        {
            _logger = logger;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _geocalculate = geocalculate;
            _userGeographicalPointRepository = userGeographicalPointRepository;
        }



            public async Task<PaginatedItemsViewModel<UserLocationListItemViewModel>> Handle(GetUserLocationQuery request, CancellationToken cancellationToken)
            {

           var query= _userGeographicalPointRepository.GetQueryable().Where(d => d.UserId == request.userId);
            var totalItems = await query.LongCountAsync();
            if (request.pageIndex<=0)
            {
                request.pageIndex = 0;
            }
            else
            request.pageIndex--;

            var itemsOnPage = await query.AsNoTracking().Skip(request.pageSize * request.pageIndex)
                .Take(request.pageSize).Select(f => new UserLocationListItemViewModel
                {
                    startPoint = new LocationView { latitude = f.StartPoint.Latitude, longitude = f.StartPoint.Longitude },
                    endPoint = new LocationView { latitude = f.EndPoint.Latitude, longitude = f.EndPoint.Longitude },
                    distance=f.Distance
                }).ToListAsync();

            return new PaginatedItemsViewModel<UserLocationListItemViewModel>(request. pageIndex,request.pageSize, totalItems, itemsOnPage);



               

        }


    }

    
}
