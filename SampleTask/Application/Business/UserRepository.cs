﻿using Microsoft.EntityFrameworkCore;
using ProjectTestMediator.Application.Context;
using SampleTask.Application.Data;
using SampleTask.Application.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Business
{

    public interface IUserRepository : IRepositoryBase<User>
    {
    }
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public override User Add(User entity)
        {
            return base.Add(entity);
        }

        
    }


}
