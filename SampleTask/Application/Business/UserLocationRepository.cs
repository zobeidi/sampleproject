﻿using Microsoft.EntityFrameworkCore;
using ProjectTestMediator.Application.Context;
using SampleTask.Application.Data;
using SampleTask.Application.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SampleTask.Application.Business
{

    public interface IUserGeographicalPointRepository : IRepositoryBase<UserGeographicalPoint>
    {
    }
    public class UserGeographicalPointRepository : RepositoryBase<UserGeographicalPoint>, IUserGeographicalPointRepository
    {
        public UserGeographicalPointRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
    }


}
