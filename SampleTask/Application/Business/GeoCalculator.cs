﻿using Geolocation;
using ProjectTestMediator.Application.Context;
using SampleTask.Application.Data;
using SampleTask.Application.Domain;
using SampleTask.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Business
{

    public interface IGeoCalculate 
    {
        double GetDistance(LocationView startPoint, LocationView endPoint);
    }
    public class GeoCalculate : IGeoCalculate
    {
        public double GetDistance(LocationView startPoint, LocationView endPoint)
        {
         return   GeoCalculator.GetDistance(startPoint.latitude, startPoint.longitude, endPoint.latitude, endPoint.longitude, 2, DistanceUnit.Meters);

        }
    }


}
