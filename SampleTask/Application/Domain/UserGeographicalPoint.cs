﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Domain
{
    [Table("UserGeographicalPoint")]
    public class UserGeographicalPoint : BaseEntity<long>
    {

        public long UserId { set; get; }
        public Location StartPoint { set; get; }
        public Location EndPoint { set; get; }
        public double Distance { set; get; }

        public virtual User User { set; get; }
    }

}
