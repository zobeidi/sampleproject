﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Domain
{
    [Table("User")]
    public class User: BaseEntity<long>
    {
        public User()
        {
            UserGeographicalPoints = new HashSet<UserGeographicalPoint>();
        }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }


        public virtual ICollection<UserGeographicalPoint> UserGeographicalPoints { set; get; }
          }
}
