﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Domain
{
    public interface IEntity
    {
        object Id { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime? ModifiedDate { get; set; }
        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
        byte[] Version { get; set; }
        bool IsActive { get; set; }

    }

    public interface IEntity<T> : IEntity
    {
        new T Id { get; set; }
    }

    public abstract class BaseEntity<T> : IEntity<T>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public T Id { get; set; }

        object IEntity.Id
        {
            get { return this.Id; }
            set { value = this.Id; }

        }


        private DateTime? createdDate;
        [DataType(DataType.DateTime)]
        public DateTime CreatedDate
        {
            get { return createdDate ?? DateTime.UtcNow; }
            set { createdDate = value; }
        }

        [DataType(DataType.DateTime)]
        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        [Timestamp]
        public byte[] Version { get; set; }
        public bool IsActive { get; set; }

        }


        ////////////////////////////////


        //public interface IDomainEntity<TPrimaryKey>
        //{
        //    TPrimaryKey Id { get; set; }

        //    bool IsActive { get; set; }
        //}
        //public interface IAuditInfo
        //{
        //    DateTime RegistrationDate { get; set; }


        //}
        //public interface ICreationAudited
        //{
        //    long? CreatorUserId { get; set; }
        //    public long? ModifiedUserId { get; set; }

        //}
        //public class BaseDomainEntity<TPrimaryKey> : IDomainEntity<TPrimaryKey>
        //{

        //    public TPrimaryKey Id { get; set; }

        //    public bool IsActive { get; set; }

        //}
        //public class DomainEntity<TPrimaryKey> : BaseDomainEntity<TPrimaryKey>, IAuditInfo, ICreationAudited
        //{
        //    [DataType(DataType.DateTime)]
        //    public DateTime RegistrationDate { get; set; }

        //    [DataType(DataType.DateTime)]
        //     public DateTime? ModifiedDate { get; set; }

        //    public long? CreatorUserId { get; set; }
        //    public long? ModifiedUserId { get; set; }


        //}


    }
