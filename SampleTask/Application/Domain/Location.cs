﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Application.Domain
{
    public class Location : ValueObject
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Latitude;
            yield return Longitude;

        }
    }
}
