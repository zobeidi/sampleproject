﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectTestMediator.Application.Context;

using SampleTask.Application.Domain;
using SampleTask.Application.Services;
using SampleTask.Infrastructure.EntityConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SampleTask.Application.Models
{
    public class SampleTaskDbContext:DbContext, IUnitOfWork
    {

        private readonly UserResolverService _userService;
       
        public SampleTaskDbContext(DbContextOptions options, UserResolverService userService) :
            base(options)
        {
            _userService = userService;
        }


        public DbSet<User> Users { get; set; }
       public DbSet<UserGeographicalPoint> UserGeographicalPoints { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }


        public void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            base.Set<TEntity>().AddRange(entities);
        }

        public void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            base.Set<TEntity>().RemoveRange(entities);
        }
        public void MarkAsChanged<TEntity>(TEntity entity) where TEntity : class
        {
            base.Entry(entity).State = EntityState.Modified; // Or use ---> this.Update(entity);
        }

        public void ExecuteSqlCommand(string query)
        {
         
            base.Database.ExecuteSqlRaw(query);
        }

        public void ExecuteSqlCommand(string query, params object[] parameters)
        {
            base.Database.ExecuteSqlRaw(query, parameters);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // it should be placed here, otherwise it will rewrite the following settings!
            builder.ApplyConfiguration(new UserGeographicalPointEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserGeographicalPointEntityTypeConfiguration());

            base.OnModelCreating(builder);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);

        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(cancellationToken);

        }



        private void OnBeforeSaving()
        {
            var entries = ChangeTracker.Entries();
            var utcNow = DateTime.UtcNow;

            foreach (var entry in entries)
            {
                // for entities that inherit from BaseEntity,
                // set UpdatedOn / CreatedOn appropriately
                //if (entry.Entity is IEntity trackable)
                //{
                //    switch (entry.State)
                //    {
                //        case EntityState.Modified:
                //            trackable.ModifiedDate = utcNow;
                //            entry.Property("CreatedOn").IsModified = false;
                //            break;
                //        case EntityState.Added:
                //            trackable.CreatedDate = utcNow;
                //            trackable.ModifiedDate = utcNow;
                //            break;
                //    }
                //}

            }
            }

      
    }

  
   
}
