﻿using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SampleTask.Application.Command;
using SampleTask.Application.Data;
using SampleTask.Application.Queries;
using SampleTask.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Controllers.V1
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("v{version:apiVersion}/[controller]")]
    public class UserLocationController : ApiControllerBase
    {

        private readonly ILogger<UserLocationController> _logger;
        private readonly IMediator _mediator;

        public UserLocationController(ILogger<UserLocationController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("add")]
       [HttpPost]
        public async Task<IActionResult> add([FromBody] RegisterUserLocationCommand model)
        {
            model.userId = User.UserId();
            var result = await _mediator.Send(model);
            return Response(result);

        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("list")]
        [HttpPost]
        public async Task<IActionResult> list( [FromBody] GetUserLocationQuery model)
        {
            GetUserLocationQuery query = new GetUserLocationQuery
            {
                userId = User.UserId(),
                pageIndex = model.pageIndex,
                pageSize = model.pageSize,
                sort = ""
            };


            var result = await _mediator.Send(query);
            return Response(result);

        }

    }
}
