﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SampleTask.Application.Command;
using SampleTask.Application.Data;
using SampleTask.Application.Models;
using SampleTask.Extensions;
using SampleTask.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Controllers.V1
{
    [ApiVersion("1.0")]
    [ApiController]

    [Route("v{version:apiVersion}/[controller]")]
    public class UserController : ApiControllerBase
    {

        private readonly ILogger<UserController> _logger;
        private readonly IMediator _mediator;

        public UserController(ILogger<UserController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost]
        [Route("register")]
        [ServiceFilter(typeof(ModelStateValidationFilter))]
        public async Task<IActionResult> Register([FromBody]RegisterUserCommand  model)
        {
      var result=  await    _mediator.Send(model);
            return Response(result);
            

        }


        [HttpPost]
        [Route("login")]
        [ServiceFilter(typeof(ModelStateValidationFilter))]
        public async Task<IActionResult> Register([FromBody] LoginUserCommand model)
        {
                var result = await _mediator.Send(model);
            return Response(result);

        }

    }
}
