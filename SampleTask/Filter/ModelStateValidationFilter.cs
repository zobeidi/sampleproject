﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SampleTask.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleTask.Filter
{
    public class ModelStateValidationFilter : Attribute, IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var param = context.ActionArguments.SingleOrDefault(p => p.Value is IViewModelBase);
            if (param.Value == null)
            {
                context.Result = new BadRequestObjectResult(new ResponseObject("اطلاعات به درستی ارسال نشده است"));
                return;
            }


            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(new ReponseModelStateError(context.ModelState,  "ValidationException",400));
            }
        }

        public void OnActionExecuted(ActionExecutedContext context) { }
    }
}
